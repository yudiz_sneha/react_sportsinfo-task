import React from 'react';
import '../styles/index.scss';
import data from '../data/sportsInfo.json'

function SportsInfo() {
  return (
        <>
        <div className="container">
          <div className="row">
            {data.data.map((add) => {
            return (
            <>
              <div className="blog">
                <div className="img">
                  <img src={add.sImage} alt="img" />
                </div>
            <div className="content">
                <div className="title">
                    <h3>{add.sTitle}</h3>
                    <p maxLength="2">{add.sDescription}</p>
                </div>
                <div className="footer">
                    <div className="leftfooter">
                    <span style={{textTransform: "uppercase",
                          color: "#2990f8",
                          fontWeight: "500",
                          cursor:"pointer",
                          fontSize:"13px"
                        }}
                    >
                        {add.iId.sFirstName} {add.iId.sLastName}
                    </span>
                    <span style={{marginLeft:"5px", color:"#626262"}}>a year ago</span>
                    </div>

                    <div className="rightfooter">
                      <img src="https://www.sports.info/comment-icon.7aef209a3b2086028430.svg" alt="" />
                    <span style={{marginLeft:"5px"}}>0</span>
                      <img src="https://www.sports.info/view-icon.b16661e96527947b18f1.svg" alt="" />
                    <span style={{marginLeft:"5px"}}>{add.nViewCounts}</span>
                    </div>
                    </div>
                </div>
              </div>
            </>
            );
          })}
        </div>
        <div className="rightrow">
          <div className="highlight"></div>
        </div>
      </div>

    </> 
  )
}

export default SportsInfo
