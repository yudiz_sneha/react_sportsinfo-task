import './App.css';
import SportsInfo from './components/SportsInfo';

function App() {
  return (
    <div>
      <SportsInfo />
    </div>
  );
}

export default App;
